package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepositoryDTO {

    void add(@NotNull final SessionDTO session);

    void update(@NotNull final SessionDTO session);

    void remove(@NotNull final SessionDTO session);

    void removeByUserId(@NotNull final String userId);

    @Nullable
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findOneById(@NotNull final String id);

    @Nullable
    SessionDTO findOneByIdUserId(@NotNull final String userId, @NotNull final String id);

}
