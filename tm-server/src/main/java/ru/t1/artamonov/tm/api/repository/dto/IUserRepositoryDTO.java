package ru.t1.artamonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepositoryDTO {

    void add(@NotNull UserDTO user);

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void remove(@NotNull UserDTO user);

    void update(@NotNull UserDTO user);

}
