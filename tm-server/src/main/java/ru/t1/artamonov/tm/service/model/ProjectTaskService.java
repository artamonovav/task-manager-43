package ru.t1.artamonov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.artamonov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.artamonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.artamonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.artamonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.repository.dto.ProjectRepositoryDTO;
import ru.t1.artamonov.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskServiceDTO {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    @Override
    public ITaskRepositoryDTO getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepositoryDTO(entityManager);
    }

    @NotNull
    @Override
    public IProjectRepositoryDTO getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepositoryDTO(entityManager);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepositoryDTO taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectRepositoryDTO projectRepository = getProjectRepository(entityManager);

        try {
            if (!projectRepository.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = Optional.ofNullable(taskRepository.findOneByIdUserId(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepositoryDTO taskRepository = getTaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeTasksByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskRepositoryDTO taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectRepositoryDTO projectRepository = getProjectRepository(entityManager);
        try {
            if (!projectRepository.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
